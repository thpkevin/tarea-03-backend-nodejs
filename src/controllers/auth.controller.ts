import {Request, Response} from 'express';
import bcrypt from 'bcryptjs';
import User from '../models/user.model';
import { NetworkResponseI } from '../domain/response.model';
import generateToken from '../utils/generateJWT.utils';

const login = async(req: Request, res: Response) => {
    let response : NetworkResponseI;
    try {
        const {email, password} = req.body;
        const userDb = await User.findOne({email});
        if(!userDb){
            response = {
                success: false,
                message: 'Usuario no existe',
            }
            return res.status(404).send(response);
        }
        //@ts-ignore
        const validPassword = bcrypt.compareSync(password,userDb.password);
        if(!validPassword){
            response = {
                success: false,
                message: 'Usuario o contraseña incorrectos'
            }
            return res.status(400).send(response);
        }
        const token = await generateToken(userDb._id);
        response = {
            success: true,
            message: 'Usuario logeado correctamente',
            data:{ token: token }
        }
        return res.send(response);

	} catch (error) {
		return res.status(500).send({
			error,
			success:false,
			message: 'Ha ocurrido un problema'
		});
	}
}

export {
    login
}