
import { NetworkResponseI } from '../domain/response.model';
import  Dish from '../models/Dish.model';
import  DishHistory from '../models/DishHistory.model';
import { Response, Request } from 'express'



const create = async(req: Request, res: Response) => {
    let response: NetworkResponseI;
    try{
        const dish = new Dish(req.body);
        await dish.save();
        response = {
            success: true,
            data: dish,
            message: 'Plato creado correctamente.',
        }
        res.send(response).status(201);

    }catch(error){
        response = {
            success: true,
            message: 'Ha ocurrido un error',
            error
        }
        res.send(response);
    }
}

const getAll = async(req: Request, res: Response) => {
    let response: NetworkResponseI;
    try {
        const dishes = await Dish.find();
        response = {
            success: true,
            data: dishes,
            message: 'Listado de platos',
        }
        res.send(response).status(200);
    } catch (error) {
        response = {
            success: false,
            error,
            message: 'Ha ocurrido un problema',
        }
        res.send(response).status(200);
    }
}

const getById = async(req: Request, res: Response) => {
    let response: NetworkResponseI;
    try {
        const { id } = req.params;
        const dish = await Dish.findById(id);
        if(dish){
            response = {
                success: true,
                message: '',
                data: dish
            }
            return res.send(response);
        }
        response = {
            success: false,
            message: 'No se ha encontrado el producto',
            error: 'Producto no encontrado'
        }
        res.status(400).send(response);
    } catch (error) {
        response = {
            success: false,
            message: 'Ha ocurrido un problema',
            error
        }
        res.status(500).send(response);
    }
}

const update = async (req, res) => {
    let response: NetworkResponseI;
    try{
        const dish = req.body;
        await Dish.updateOne({_id: dish._id}, {...dish})
        response = {
            success: true,
            data: null,
            message: 'Plato actualizado correctamente'
        }
        res.send(response);
    }catch (error) {
        response = {
            success: false,
            message: 'Ha ocurrido un problema',
            error
        }
        res.status(500).send(response);
    }
}

// TODO : agregar logic delete
const deleteDish = async (req: Request, res: Response) => {
    let response: NetworkResponseI;
    try {

        const id = req.body;
        const dish = await Dish.findById(id);
        const dishHistory = new DishHistory({
            '_id': dish['_id'],
            'name': dish['name'], 
            'picture': dish['picture'],
            'description': dish['description'],
            'price': dish['price']
        });
        await dishHistory.save();
        await Dish.deleteOne({_id: id});
        response = {
            success: true,
            message: 'Plato eliminado correctamente'
        }
        res.send(response);
    } catch (error) {
        response = {
            success: false,
            message: 'Ha ocurrido un problema',
            error
        }
        res.status(500).send(response);
    }
}

export{
    create,
    getAll,
    getById,
    update,
    deleteDish
}

/*
1xx

2xx respuestas correcta
200 - respuesta correctamente
201 - Creacion correcta

4xx

5xx
*/