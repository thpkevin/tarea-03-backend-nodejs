import User from '../models/user.model';
import bcrypt from 'bcryptjs';

const createUser = async (request, response) => {
	try {
		const {email, name, lastName, password} = request.body;
		if(email && name && lastName && password){
			const user = new User(request.body);
			const salt = bcrypt.genSaltSync();

			//@ts-ignore
			user.password = bcrypt.hashSync(password, salt);

			await user.save();
			response.status(201).send({
				success: true,
				message: 'Usuario creado correctamente'
			});
			return;
		}
		return response.status(400).send({
			success:false,
			message: 'Datos invalidos'
		});
	} catch (error) {
		return response.status(500).send({
			error,
			success:false,
			message: 'Ha ocurrido un problema'
		});
	}
}

export { createUser }