import express from 'express';
import cors from 'cors';
import { config } from 'dotenv';
import dbConnection from './database/config';
import userRoutes from './routes/user.routes';
import dishRoutes from './routes/dish.routes';
import authRouter from './routes/auth.route';

config();
dbConnection();

const app = express();


app.use(cors());
app.use(express.json());

app.use('/api/users', userRoutes )
app.use('/api/dish', dishRoutes )
app.use('/api/auth', authRouter );

app.listen(process.env.PORT, () => {
	console.log(`Server is running on port ${process.env.PORT}`);
})

// TODO Añadir una collecion más aparte de la de usuario

/* TYPES - TypeScript
	number;
	boolean;
	strings;
	null
	any
*/

// let usuario = {nombre: "ismael"};

// usuario = 123;
// usuario = "123456";
// usuario = false;

//let numeros:  number[] = [1,2,3,4,5];
//let respuesta: any[] = [1,{a: 1}, false, null, "hola"];
