import { validationResult } from 'express-validator';

const fieldValidator = (req, res, next) => {
    const errors = validationResult(req);

    if(!errors.isEmpty()){
        return res.status(400).send({
            success: false,
            code: 1,
            errors: errors.mapped(),
            message: 'Faltan datos.'
        })
    }
    next();
}


export{
    fieldValidator
}