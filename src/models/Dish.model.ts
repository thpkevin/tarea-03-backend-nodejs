import { Schema, model} from 'mongoose'

const DishSchema = new Schema({
    picture:{
        type: String,
        required: true
    },
    name:{
        type: String,
        required: true
    },
    price:{
        type: Number,
        required: true
    },
    description:{
        type: String,
        required: true
    },
    offert:{
        type: Schema.Types.Map,
        required: false
    },
    isOffert:{
        type: Boolean,
        default: false,
        required: false
    },
    delivered:{
        type: Schema.Types.Number,
        default: 0,
        required: false
    }
})

export default model('dish', DishSchema);
