import { Schema, model} from 'mongoose'

const DishHistorySchema = new Schema({
    picture:{
        type: String,
        required: true
    },
    name:{
        type: String,
        required: true
    },
    price:{
        type: Number,
        required: true
    },
    description:{
        type: String,
        required: true
    },
    offert:{
        type: Schema.Types.Map,
        required: false
    },
    isOffert:{
        type: Boolean,
        default: false,
        required: false
    },
    delivered:{
        type: Schema.Types.Number,
        default: 0,
        required: false
    }
}/*,{
    collection: 'dish_history'
}*/)

export default model('dish_history', DishHistorySchema);
