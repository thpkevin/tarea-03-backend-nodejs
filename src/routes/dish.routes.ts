
import { Router } from 'express';
import { create, deleteDish, getAll, getById, update } from '../controllers/dish.controller';
import { check } from 'express-validator';
import { fieldValidator } from '../middlewares/dish.moddleware';

const router = Router();

router.post('/', [
    check('name', 'El campo nombre es requerido').notEmpty(),
    check('description'),
    check('picture'),
    check('price'),
    fieldValidator
],create);

router.get('/', getAll);
router.get('/:id', getById);
router.patch('/', update);
router.delete('/', deleteDish);

export default router;