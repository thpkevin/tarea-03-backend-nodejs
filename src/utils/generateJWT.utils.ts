import jwt from 'jsonwebtoken';

const generateJWT = (uid: string) => {
    return new Promise((resolve, reject) => {
        const payload = { uid };
        jwt.sign(payload, process.env.JWT_KEY,{
            expiresIn: '24H'
        },(err, token) => {
            if(err){
                reject({message: 'El token no pudo ser generado'});
                return;
            }
            resolve(token);
            return;
        });
    })
}

export default generateJWT;